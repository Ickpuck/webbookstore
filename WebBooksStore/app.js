﻿var MainApp = angular.module('MainApp', ['ngRoute', 'rzModule', 'ngMessages', 'ui.mask']);

MainApp.config(function ($routeProvider, $locationProvider, $httpProvider) {
        
    $routeProvider
        .when('/home', {
            templateUrl: 'Views/Home.html',
            controller: 'HomeController'
        })
        .when('/category/:id', {
            templateUrl: 'Views/Category.html',
            controller: 'CategoryController'
        })
        .when('/item/:id', {
            templateUrl: 'Views/Item.html',
            controller: 'ItemController'
        })
        .when('/search', {
            templateUrl: 'Views/Category.html',
            controller: 'IndexController'
        })
        .when('/cart', {
            templateUrl: 'Views/Cart.html',
            controller: 'CartController'
        })
        .otherwise({
            redirectTo: '/home'
        });

    $locationProvider.html5Mode(true);
});

MainApp.factory('LocalStorageManager', function () {

    var LocalStorageManager = {
        set: function (key, value) {
            window.localStorage.setItem(key, JSON.stringify(value));
        },
        get: function (key) {
            try {
                return JSON.parse(window.localStorage.getItem(key));
            } catch (e) {

            }
        },
        clear: function () {
            window.localStorage.clear();
        },
        remove: function (key) {
            window.localStorage.removeItem(key);
        }
    };

    return LocalStorageManager;
});

MainApp.run(function ($rootScope) {

    $rootScope.priceRanger = {
        min: 1,
        max: 10000,
        options: {
            floor: 0,
            ceil: 10000,
            step: 100,
            translate: function (value) {
                return '\u20bd' + value;
            }
        }
    }
    $rootScope.yearRanger = {
        min: 1990,
        max: 2018,
        options: {
            floor: 1990,
            ceil: 2018,
            step: 1
        }
    }
    $rootScope.playersRanger = {
        min: 1,
        max: 9,
        options: {
            floor: 1,
            ceil: 9,
            step: 1
        }
    }
    $rootScope.pagesRanger = {
        min: 100,
        max: 3000,
        options: {
            floor: 100,
            ceil: 3000,
            step: 150
        }
    }
});

MainApp.directive('ngLoading', function () {
    return {
        restrict: 'E',
        template: '<div class="form-group"><img src="https://media.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif" width="50" height="50" /></div>',
        scope: {
            loadingData: '='
        },
        link: function (scope, element, attrs) {
            scope.$watch('loadingData', function (val) {
                if (val) {
                    element.hide();
                }
                else {
                    element.show();
                }
            });
        }
    }
});
