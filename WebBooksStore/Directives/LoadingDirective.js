﻿angular.module('MainApp')
  .directive('loading', function () {
      return {
          restrict: 'E',
          replace: true,
          template: '<div><img src="http://www.nasa.gov/multimedia/videogallery/ajax-loader.gif" width="20" height="20" /></div>',
          link: function (rootScope, element, attr) {
              rootScope.$watch('loading', function (val) {
                  if (val)
                      $(element).show();
                  else
                      $(element).hide();
              });
          }
      }
  })