﻿var app = angular.module('MainApp');
app.factory('authService', ['$http', '$q', 'LocalStorageManager', function ($http, $q, LocalStorageManager) {

    var serviceBase = 'http://frozzentime.hldns.ru:3000/';
    var authServiceFactory = {};

    var _authentication = {
        isAuth: false,
        userId: "",
        access_token: "",
        userName: "",
        FIO: ""
    };

    var _login = function (loginData) {

        var deferred = $q.defer();

        $http.post(serviceBase + 'api/customers/login', loginData).then(function (response) {
            var userData = {
                access_token: null,
                userName: null,
                userId: null
            }
            userData.access_token = response.data.id;
            userData.userName = loginData.username;
            userData.userId = response.data.userId;
            $http.get(serviceBase + 'api/customers/' + response.data.userId + '?access_token=' + userData.access_token).then(function (response) {
                LocalStorageManager.set('authData', {
                    access_token: userData.access_token,
                    userName: loginData.username,
                    userId: userData.userId,
                    FIO: response.data.firstName + " " + response.data.lastName
                });
                _authentication.FIO = response.data.firstName + " " + response.data.lastName;
            });
            
            _authentication.isAuth = true;
            _authentication.userName = loginData.username;
            
            deferred.resolve(response);

        }, function (err) {
            _logOut();
            deferred.reject(err);
        });

        return deferred.promise;

    };

    var _logOut = function () {
        LocalStorageManager.remove('authData');

        _authentication.isAuth = false;
        _authentication.userName = "";
        _authentication.access_token = "";
        _authentication.userId = "";
        _authentication.FIO = "";
    };

    var _fillAuthData = function () {

        var authData = LocalStorageManager.get('authData');
        if (authData) {
            _authentication.access_token = authData.access_token;
            _authentication.userId = authData.userId;
            _authentication.isAuth = true;
            _authentication.userName = authData.userName;
            _authentication.FIO = authData.FIO;
        }
    }

    var _getAuthentication = function () {
        var authData = LocalStorageManager.get('authData');
        return authData;
    }

    authServiceFactory.login = _login;
    authServiceFactory.logOut = _logOut;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.authentication = _authentication;

    return authServiceFactory;
}]);