﻿angular.module('MainApp').
    controller('HomeController', function ($q, $rootScope, $scope, $http) {

        $http.get('http://frozzentime.hldns.ru:3000/api/items').then(function (response) {
            var items = response.data;
            var promises = [];
            var tags = [];
            angular.forEach(items, function (value) {
                promises.push($http.get('http://frozzentime.hldns.ru:3000/api/items/' + value.id + '/tags'));
            });
            $q.all(promises).then(function (results) {
                for (var i = 0; i < results.length; i++) {
                    items[i].tags = results[i].data;
                }
                $scope.items = items;
            });
        });

        $scope.tagClick = function (tag) {
            $http.get('http://frozzentime.hldns.ru:3000/api/tags/' + tag.id + '/items').then(function (response) {
                var items = response.data;
                var promises = [];
                var tags = [];
                angular.forEach(items, function (value) {
                    promises.push($http.get('http://frozzentime.hldns.ru:3000/api/items/' + value.id + '/tags'));
                });
                $q.all(promises).then(function (results) {
                    for (var i = 0; i < results.length; i++) {
                        items[i].tags = results[i].data;
                    }
                    $scope.items = items;
                });
            });
        }
    });