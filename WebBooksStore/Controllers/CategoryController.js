﻿angular.module('MainApp')
    .controller('CategoryController', function ($q, $http, $scope, $routeParams, $rootScope, $route, $location) {
        if (url != 'search') {
            $http.get('http://frozzentime.hldns.ru:3000/api/categories/' + $routeParams.id + '/items').then(function (response) {
                $scope.items = response.data;
                $scope.routeId = $routeParams.id;

                var promises = [];
                var tags = [];
                angular.forEach($scope.items, function (value) {
                    promises.push($http.get('http://frozzentime.hldns.ru:3000/api/items/' + value.id + '/tags'));
                });
                $q.all(promises).then(function (results) {
                    //console.log(results);
                    for (var i = 0; i < results.length; i++) {
                        $scope.items[i].tags = results[i].data;
                    }
                    //console.log($scope.items);
                });
            });
        }
        var url = $location.path().split('/')[1];
        $scope.url = $location.path().split('/')[1];
        $scope.searchName = "";
        $scope.author = "";
        $scope.issuer = "";

        $scope.filterGames = function () {
            var jsonFilter = JSON.parse(
                '{ "where": { "and": [' +
                    '{ "price": { "lt": "' + ($rootScope.priceRanger.max + 100) + '" } },' +
                    '{ "price": { "gt": "' + ($rootScope.priceRanger.min - 100) + '" } },' +
                    '{ "name": { "like": "' + $scope.searchName + '" } }] } }'// +
                    //'{"properties.Колличество человек (макс)":{"lt":"' + ($rootScope.playersRanger.max+1) + '"}},' +
                    //'{"properties.Колличество человек (мин)":{"gt":"' + ($rootScope.playersRanger.min-1) + '"}}] } }'
                );
            //console.log(JSON.stringify(jsonFilter));
            $http.get('http://frozzentime.hldns.ru:3000/api/categories/' + $routeParams.id + '/items?filter=' + JSON.stringify(jsonFilter)).then(function (response) {
                $scope.items = response.data;

                var promises = [];
                var tags = [];
                angular.forEach($scope.items, function (value) {
                    promises.push($http.get('http://frozzentime.hldns.ru:3000/api/items/' + value.id + '/tags'));
                });
                $q.all(promises).then(function (results) {
                    //console.log(results);
                    for (var i = 0; i < results.length; i++) {
                        $scope.items[i].tags = results[i].data;
                    }
                    //console.log($scope.items);
                });
            });
        }
        $scope.filterBooks = function () {
            var jsonFilter = JSON.parse(
                '{ "where": { "and": [' +
                    '{ "price": { "lt": "' + ($rootScope.priceRanger.max-100) + '" } },' +
                    '{ "price": { "gt": "' + ($rootScope.priceRanger.min+100) + '" } },' +
                    '{ "name": { "like": "' + $scope.searchName + '" } },' +
                    '{"properties.Издательство":{"like":"' + $scope.issuer + '"}},' +
                    '{ "properties.Автор": { "like": "' + $scope.author + '" } },' +
                    '{"properties.Год издания":{"lt":"' + ($rootScope.yearRanger.max+1) + '"}},' +
                    '{"properties.Год издания":{"gt":"' + ($rootScope.yearRanger.min-1) + '"}}] } }'
                );

            $http.get('http://frozzentime.hldns.ru:3000/api/categories/' + $routeParams.id + '/items?filter=' + JSON.stringify(jsonFilter)).then(function (response) {
                $scope.items = response.data;

                var promises = [];
                var tags = [];
                angular.forEach($scope.items, function (value) {
                    promises.push($http.get('http://frozzentime.hldns.ru:3000/api/items/' + value.id + '/tags'));
                });
                $q.all(promises).then(function (results) {
                    //console.log(results);
                    for (var i = 0; i < results.length; i++) {
                        $scope.items[i].tags = results[i].data;
                    }
                    //console.log($scope.items);
                });
            });
        }
    });