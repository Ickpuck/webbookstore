﻿angular.module('MainApp')
    .controller('LoginController', function ($scope, authService, $timeout) {
        $scope.loginData = {
            username: "",
            password: ""
        }
        $scope.loading = false;
        $scope.show;
        $scope.login = function () {
            $scope.loading = true;
            authService.login($scope.loginData).then(function (response) {
                $scope.message = "";
                $scope.loading = false;
                angular.element('#loginModal').modal('hide');
            }, function (err) {
                $scope.loading = false;
                $scope.message = "Неверный логин или пароль";
            });
        }
    });