﻿angular.module('MainApp')
    .controller('CartController', function ($q, $scope, $http, CartFactory, authService, $timeout) {
        authService.fillAuthData();
        $scope.cartItems = CartFactory.getItems();
        $scope.submited = true;
        $scope.message = "";
        var allSuccessfull = true;
        $http.get('http://frozzentime.hldns.ru:3000/api/customers/' + authService.authentication.userId + '?access_token=' + authService.authentication.access_token).then(function (response) {
            $scope.order.address = response.data.address;
            $scope.order.contactPhone = response.data.phone;
        });

        $scope.removeFromCart = function (item) {
            for (var i = 0; i < $scope.cartItems.length; i++) {
                if ($scope.cartItems[i].id == item.id) {
                    $scope.cartItems.splice(i, 1);
                    break;
                }
            }
            CartFactory.setItems($scope.cartItems);
        }
        $scope.incCount = function (item) {
            item.count++;
            CartFactory.setItems($scope.cartItems);
        }
        $scope.decCount = function (item) {
            item.count--;
            CartFactory.setItems($scope.cartItems);
        }

        $scope.clearCart = function () {
            CartFactory.clear();
            $scope.cartItems = null;
        }
        $scope.getSum = function () {
            var sum = 0;
            if ($scope.cartItems) {
                for (var i = 0; i < $scope.cartItems.length; i++) {
                    sum += $scope.cartItems[i].count * $scope.cartItems[i].price;
                }
            }
            return sum;
        }
        $scope.submitOrder = function () {
            $scope.submited = null;
            var orderId;
            $scope.order.customerId = authService.authentication.userId;
            $scope.order.totalSum = $scope.getSum();
            
            $http.post('http://frozzentime.hldns.ru:3000/api/orders?access_token=' + authService.authentication.access_token, $scope.order).then(function (response) {
                $scope.message = null;
                console.log(response);
                orderId = response.data.id;
                var promises = [];
                angular.forEach($scope.cartItems, function (item) {                    
                    var orderItem = {
                        price: 0,
                        count: 0,
                        fullPrice: 0,
                        itemId: "",
                        orderId: ""
                    }
                    orderItem.count = item.count;
                    orderItem.price = item.price;
                    orderItem.fullPrice = item.count * item.price;
                    orderItem.itemId = item.id;
                    orderItem.orderId = orderId;
                    console.log(orderItem);

                    promises.push($http.post('http://frozzentime.hldns.ru:3000/api/orderitems?access_token=' + authService.authentication.access_token, orderItem));
                });
                $q.all(promises).then(function (results) {
                    var itemsToRemove = [];
                    for (var i = 0; i < results.length; i++) {
                        if (results[i].status == 200) {
                            var item = { id: null };
                            item.id = results[i].data.itemId;
                            itemsToRemove.push(item);
                        }
                        else {
                            allSuccessfull = false;
                        }
                    }
                    if (allSuccessfull) {
                        $scope.message = "Ваш заказя принят!"
                        $timeout(function () {
                            $scope.clearCart();
                            $scope.submited = true;
                        }, 4000);
                    }
                    else {
                        angular.forEach(itemsToRemove, function (item) {
                            $scope.removeFromCart(item);
                        });
                        $scope.submited = true;
                        $scope.message = "К сожалению, что-то пошло не так... Ваш заказ оформлен частично. Пожалуйста, попробуйте отправить оставшиеся товары повторно. Спасибо!";
                    }
                });

            }, function (err) { console.log(err) });               
        }

        $scope.order = {
            totalSum: 0,
            createDate: Date.now(),
            status: "",
            address: "",
            deliveryAt: Date.now(),
            pickupAt: Date.now(),
            contactPhone: "",
            tracking: false,
            customerId: ""
        }        

    });

angular.module('MainApp')
    .factory('CartFactory', function (LocalStorageManager) {
        var CartFactory = {};
        var _items = [];

        var _fillData = function () {
            _items = LocalStorageManager.get('CartItems');
        }
        var _addToCart = function (orderItem) {
            _fillData();
            if (_items) {
                var newItem = true;
                for (var i = 0; i < _items.length; i++) {
                    if (_items[i].id == orderItem.id) {
                        if (_items[i].count < _items[i].left) {
                            _items[i].count++;
                        }
                        newItem = false;
                        break;
                    }
                }
                if (newItem)
                    _items.push(orderItem);
            }
            else {
                console.log('items inited');
                _items = [];
            }
            if (_items.length < 1)
                _items.push(orderItem);
            LocalStorageManager.set('CartItems', _items);
        }
        var _getItems = function () {
            _fillData();
            return _items;
        }
        var _clear = function () {
            LocalStorageManager.remove('CartItems');
        }
        var _setItems = function (items) {
            LocalStorageManager.set('CartItems', items);
        }
        var _cartItemsCount = function () {
            _fillData();
            var count = 0;
            if (_items) {
                for (var i = 0; i < _items.length; i++) {
                    count += _items[i].count;
                }
            }
            return count;
        }

        CartFactory.fillData = _fillData;
        CartFactory.addToCart = _addToCart;
        CartFactory.getItems = _getItems;
        CartFactory.clear = _clear;
        CartFactory.setItems = _setItems;
        CartFactory.cartItemsCount = _cartItemsCount;

        return CartFactory;
    });