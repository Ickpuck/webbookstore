﻿angular.module('MainApp')
    .controller('RegisterController', function ($scope, $http) {
        $scope.user = {
            address: "",
            phone: "",
            firstName: "",
            surName: "",
            lastName: "",
            username: "",
            email: ""
        }
        $scope.loading = false;
        $scope.show;
        $scope.register = function () {
            $scope.loading = true;
            $http.post('http://frozzentime.hldns.ru:3000/api/customers/', $scope.user).then(function (success) {
                console.log('Success register', success);
                $scope.message = "Регистрация выполнена успешно!";
                $scope.loading = false;
            }, function (error) {
                $scope.message = "Регистрация не удалась!";
                console.log('Failed register', error);
                $scope.loading = false;
            });
            console.log($scope.user);
        }
    });