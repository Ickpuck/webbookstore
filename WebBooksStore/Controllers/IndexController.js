﻿angular.module('MainApp')
    .controller('IndexController', function ($http, $scope, $routeParams, $location, $rootScope, authService, CartFactory, $route) {
        authService.fillAuthData();
        $scope.authentication = authService.authentication;        
        $scope.logOut = function () {
            CartFactory.clear();
            authService.logOut();
            $scope.cartItemsCount = 0;
            $location.path('/home');
        }
        $scope.cartItemsCount = CartFactory.cartItemsCount();
        $scope.url = $location.path().split('/')[1];
        $scope.searchFilter = "";
        $scope.addToCart = function (item) {
            var cartItem = {
                id: item.id,
                left: item.left,
                name: item.name,
                price: item.price,
                count: 1
            }
            CartFactory.addToCart(cartItem);
            $scope.cartItemsCount = CartFactory.cartItemsCount();
        }
        $scope.toMain = function () {
            $route.reload();
        }

        //if ($scope.searchFilter == "") {
        //    console.log($scope.searchFilter);
        //    LoadingScreenFactory.loading();
        //    $http.get('http://frozzentime.hldns.ru:3000/api/items').then(function (response) {
        //        $scope.items = response.data;
        //        LoadingScreenFactory.finishLoading();
        //    }, function (err) {
        //        LoadingScreenFactory.finishLoading();
        //    });
        //}
        $scope.search = function () {
            var stringFilter = JSON.parse('{"where":{"or":[{"name":{"like":"' + $scope.searchFilter + '"}}]}}');
            //console.log(JSON.stringify(stringFilter));
            $http.get('http://frozzentime.hldns.ru:3000/api/items?filter=' + JSON.stringify(stringFilter)).then(function (response) {
                $scope.items = response.data;

                var promises = [];
                var tags = [];
                angular.forEach($scope.items, function (value) {
                    promises.push($http.get('http://frozzentime.hldns.ru:3000/api/items/' + value.id + '/tags'));
                });
                $q.all(promises).then(function (results) {
                    //console.log(results);
                    for (var i = 0; i < results.length; i++) {
                        $scope.items[i].tags = results[i].data;
                    }
                    //console.log($scope.items);
                });
            });
            $location.path('/search');
        }
    });